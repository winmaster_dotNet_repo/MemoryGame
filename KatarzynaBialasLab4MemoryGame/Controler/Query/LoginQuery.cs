﻿using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Query
{
    public class LoginQuery : ReadRepository<Gamer>
    {
        public LoginQuery(KatarzynaBialas context) : base(context)
        {
        }

        public bool CheckCorrectLogin(string nick, string password)
        {
            List<Gamer> checkList = new List<Gamer>();
            checkList= _context.Set<Gamer>().Where(x => x.Nick == nick && x.Password==password).ToList();

            if (checkList.Count == 1)
                return true;
            else return false;
        }
    }
}
