﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormChooseType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSmall = new System.Windows.Forms.Button();
            this.buttonMedium = new System.Windows.Forms.Button();
            this.buttonBig = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Font = new System.Drawing.Font("Forte", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType.Location = new System.Drawing.Point(12, 34);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(260, 26);
            this.labelType.TabIndex = 0;
            this.labelType.Text = "Wybierz rozmiar planszy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // buttonSmall
            // 
            this.buttonSmall.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.buttonSmall.Location = new System.Drawing.Point(36, 82);
            this.buttonSmall.Name = "buttonSmall";
            this.buttonSmall.Size = new System.Drawing.Size(188, 45);
            this.buttonSmall.TabIndex = 2;
            this.buttonSmall.Text = "4x4";
            this.buttonSmall.UseVisualStyleBackColor = false;
            this.buttonSmall.Click += new System.EventHandler(this.buttonSmall_Click);
            // 
            // buttonMedium
            // 
            this.buttonMedium.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.buttonMedium.Location = new System.Drawing.Point(36, 133);
            this.buttonMedium.Name = "buttonMedium";
            this.buttonMedium.Size = new System.Drawing.Size(188, 45);
            this.buttonMedium.TabIndex = 3;
            this.buttonMedium.Text = "4x5";
            this.buttonMedium.UseVisualStyleBackColor = false;
            this.buttonMedium.Click += new System.EventHandler(this.buttonMedium_Click);
            // 
            // buttonBig
            // 
            this.buttonBig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonBig.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.buttonBig.Location = new System.Drawing.Point(36, 184);
            this.buttonBig.Name = "buttonBig";
            this.buttonBig.Size = new System.Drawing.Size(188, 45);
            this.buttonBig.TabIndex = 4;
            this.buttonBig.Text = "6x6";
            this.buttonBig.UseVisualStyleBackColor = false;
            this.buttonBig.Click += new System.EventHandler(this.buttonBig_Click);
            // 
            // FormChooseType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.buttonBig);
            this.Controls.Add(this.buttonMedium);
            this.Controls.Add(this.buttonSmall);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelType);
            this.Name = "FormChooseType";
            this.Text = "Typ rozgrywki";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSmall;
        private System.Windows.Forms.Button buttonMedium;
        private System.Windows.Forms.Button buttonBig;
    }
}