﻿using KatarzynaBialasLab4MemoryGame.Controler.Query;
using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormMember : Form
    {
        private readonly KatarzynaBialas _context;
        private LoginQuery _loginQuery;
        public static string nick;

        public FormMember()
        {
            _context = new KatarzynaBialas();
            _loginQuery = new LoginQuery(_context);
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            nick = textBoxNick.Text;
            string password = textBoxPassword.Text;
            bool correctLogin= _loginQuery.CheckCorrectLogin(nick, password);

            if(correctLogin)
            {
                MessageBox.Show("Pomyślnie zalogowano jako: " + nick, "Witaj");
                FormChooseType formChooseType = new FormChooseType();
                formChooseType.ShowDialog();
            }
            else
            {
                MessageBox.Show("Podanych danych nie ma  w bazie", "Ups..");
                Close();
            }            
        }
    }
}
