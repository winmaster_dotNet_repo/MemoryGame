﻿using KatarzynaBialasLab4MemoryGame.Controler.Command;
using KatarzynaBialasLab4MemoryGame.Controler.Command.Interfaces;
using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormNewMember : Form
    {
        private IWriteRepository<Gamer> _gamerCommand;
        private readonly KatarzynaBialas _context;

        public FormNewMember()
        {
            _context = new KatarzynaBialas();
            _gamerCommand = new WriteRepository<Gamer>(_context);
            InitializeComponent();
        }

        private void buttonAddNewMember_Click(object sender, EventArgs e)
        {
            string nick=textBoxNick.Text;
            string password = textBoxPassword.Text;
            string confirm = textBoxConfirmPassword.Text;
         
            if(nick!="" && password!="" && confirm!="")
            {
                if(password.Equals(confirm))
                {
                    Gamer gamer = new Gamer() //szybka inicjalizacja obiektu
                    {
                        Nick = nick,
                        Password = password,
                        Results = null
                    };
                    _gamerCommand.Create(gamer);

                    MessageBox.Show("Pomyślnie utworzono konto.");
                    Close();
               }

                else
                {
                    MessageBox.Show("Podane hasła są niepoprawne!");
                }
            }
            else
            {
                MessageBox.Show("Błędne dane!");
            }
        }
    }
}
