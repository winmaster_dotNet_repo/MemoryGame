﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoginMember = new System.Windows.Forms.Button();
            this.buttonLoginNewMember = new System.Windows.Forms.Button();
            this.buttonStats = new System.Windows.Forms.Button();
            this.labelGameTitle = new System.Windows.Forms.Label();
            this.pictureBoxBrain = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBrain)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLoginMember
            // 
            this.buttonLoginMember.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonLoginMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonLoginMember.Location = new System.Drawing.Point(44, 83);
            this.buttonLoginMember.Name = "buttonLoginMember";
            this.buttonLoginMember.Size = new System.Drawing.Size(170, 46);
            this.buttonLoginMember.TabIndex = 0;
            this.buttonLoginMember.Text = "Zaloguj gracza";
            this.buttonLoginMember.UseVisualStyleBackColor = false;
            this.buttonLoginMember.Click += new System.EventHandler(this.buttonLoginMember_Click);
            // 
            // buttonLoginNewMember
            // 
            this.buttonLoginNewMember.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonLoginNewMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonLoginNewMember.Location = new System.Drawing.Point(44, 135);
            this.buttonLoginNewMember.Name = "buttonLoginNewMember";
            this.buttonLoginNewMember.Size = new System.Drawing.Size(170, 46);
            this.buttonLoginNewMember.TabIndex = 1;
            this.buttonLoginNewMember.Text = "Załóż nowy profil";
            this.buttonLoginNewMember.UseVisualStyleBackColor = false;
            this.buttonLoginNewMember.Click += new System.EventHandler(this.buttonLoginNewMember_Click);
            // 
            // buttonStats
            // 
            this.buttonStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonStats.Location = new System.Drawing.Point(44, 187);
            this.buttonStats.Name = "buttonStats";
            this.buttonStats.Size = new System.Drawing.Size(170, 46);
            this.buttonStats.TabIndex = 2;
            this.buttonStats.Text = "Statystyki";
            this.buttonStats.UseVisualStyleBackColor = false;
            this.buttonStats.Click += new System.EventHandler(this.buttonStats_Click);
            // 
            // labelGameTitle
            // 
            this.labelGameTitle.AutoSize = true;
            this.labelGameTitle.Font = new System.Drawing.Font("Forte", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGameTitle.Location = new System.Drawing.Point(24, 27);
            this.labelGameTitle.Name = "labelGameTitle";
            this.labelGameTitle.Size = new System.Drawing.Size(238, 26);
            this.labelGameTitle.TabIndex = 3;
            this.labelGameTitle.Text = "Witaj w grze Memory!";
            // 
            // pictureBoxBrain
            // 
            this.pictureBoxBrain.Image = global::KatarzynaBialasLab4MemoryGame.Properties.Resources.memory1;
            this.pictureBoxBrain.Location = new System.Drawing.Point(66, 254);
            this.pictureBoxBrain.Name = "pictureBoxBrain";
            this.pictureBoxBrain.Size = new System.Drawing.Size(134, 112);
            this.pictureBoxBrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBrain.TabIndex = 0;
            this.pictureBoxBrain.TabStop = false;
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(286, 378);
            this.Controls.Add(this.pictureBoxBrain);
            this.Controls.Add(this.labelGameTitle);
            this.Controls.Add(this.buttonStats);
            this.Controls.Add(this.buttonLoginNewMember);
            this.Controls.Add(this.buttonLoginMember);
            this.Name = "FormLogin";
            this.Text = "Logowanie";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBrain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoginMember;
        private System.Windows.Forms.Button buttonLoginNewMember;
        private System.Windows.Forms.Button buttonStats;
        private System.Windows.Forms.Label labelGameTitle;
        private System.Windows.Forms.PictureBox pictureBoxBrain;
    }
}

